# Markov Chain

Program takes a text(big) as indata and creates a text of similar structure as output.


## Getting Started

### Dependencies

* Java (It runs on 3 billion devices)

### Installing

* Clone the repo
* Add a text file in the root directory.
* Specify the name of the file in the first line in the main method.

### Executing program

```
javac sim.java
```
```
java sim
```
* Follow instructions in the gui.


## Acknowledgments

* Andrey Markov