package markovFolklore;
import java.util.HashMap;
import java.util.Random;

public class Node {
	
	private String data;
	private HashMap<Node, Double> vertex = new HashMap<>();
	private static Random r = new Random();
	/**
	 * Number of successors in the text.
	 */
	private double nSucc = 0;
	
	public Node(String data){
		this.data = data;
	}
	
	public Node addVertex(Node n){
		
		nSucc += 1;	
		if(vertex.containsKey(n)){
			vertex.put(n, vertex.get(n) + 1);
		} else {
			vertex.put(n, 1.0);
		}
		
		
		return n;
	}
	
	public Node traverse(){
		
		double random = r.nextDouble();
		double compare = 0;
		
		for(Node n : vertex.keySet()){
			compare += vertex.get(n)/nSucc;
			if(compare > random){
				return n;
			}
		}
		return null;
	}
	
	public String getData(){
		return data;
	}

	/**
	 * Return the last letter.
	 */
	@Override
	public String toString() {
		return data.substring(data.length() - 1);
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		return true;
	}
	
	

}
