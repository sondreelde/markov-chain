package markovFolklore;
import java.util.ArrayList;

public class Graph {
	
	private ArrayList<Node> nodes = new ArrayList<>();
	private Node startNode = null;
	
	public void addNode(String s1, String s2){
		
		Node n1 = findNode(s1);
		Node n2 = findNode(s2);
		
		
		n1.addVertex(n2);		
		
	}
	
	private Node findNode(String s){
		for(Node n : nodes){
			if(s.equals(n.getData())){
				return n;
			}
		}
		Node newNode = new Node(s);
		if(startNode == null){
			startNode = newNode;
		}
		nodes.add(newNode);
		return newNode;
	}
	
	public Node traverse(){
		return traverse(startNode);
	}
	
	public Node traverse(String s1){
		
		Node n1 = findNode(s1);
		Node n2 = traverse(n1);
		
		if(n2 == null){
			return null;
		}
		
		return n2;
	}

	private Node traverse(Node n1) {
		
		return n1.traverse();
	}
	
	public Node getStartNode(){
		return startNode;
	}

}
