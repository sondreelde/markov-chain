package markovFolklore;
import java.util.ArrayList;
import java.util.Random;


public class ZerothOrderGraph {
	
	private ArrayList<String> nodes = new ArrayList<>();
	private static Random r = new Random();
	
	public void addNode(String s){
		nodes.add(s);
		
	}
	
	public String getRandom(){
		return nodes.get(r.nextInt(nodes.size()));
	}

}
