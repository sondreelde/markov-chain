package markovFolklore;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Reader {

	/**
	 * Reads from a file to lower case. 
	 * @param path Path to file.
	 * @return Text from file
	 */
	public static String readToLowerCase(String path) {
		try {

			BufferedReader br = new BufferedReader(new FileReader(path));

			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
		        line = br.readLine();
			}

			String ret = sb.toString();
			ret = ret.toLowerCase();
			br.close();

			return ret;

		} catch (FileNotFoundException e) {
			System.out.println("File not found. Class reader.");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	

}
