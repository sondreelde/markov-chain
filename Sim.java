package markovFolklore;
import java.util.Scanner;

public class Sim {

	public static int order;
	public static int textLenght;

	public static void main(String[] args) {
		String path = "tekst.txt";
		String txt = read(path);
		txt = txt.replace("\n", "").replace("\r", "");

		settings();

		if (order == 0) {
			ZerothOrderGraph g0 = makeZerothOrderGraph(txt);
			
			generateZerothOrderText(g0);
		} else {
			Graph g = makeGraph(txt);

			generateText(g);
		}

	}

	private static void settings() {
		Scanner scan = new Scanner(System.in);

		System.out.println("Order: (0,1,2,3)");
		order = scan.nextInt();

		System.out.println("Max lenght of the generated text: ");
		textLenght = scan.nextInt();

		scan.close();

	}

	public static String read(String s) {
		return Reader.readToLowerCase(s);
	}

	public static Graph makeGraph(String txt) {

		Graph g = new Graph();

		for (int i = 0; i + order < txt.length(); i++) {
			g.addNode(txt.substring(i, (i + order)), txt.substring((i + 1), (i + 1 + order)));
		}

		return g;

	}

	public static void generateText(Graph g) {

		Node n1 = g.getStartNode();
		System.out.print(n1.getData());
		Node n2 = null;
		int lineLength = 1;

		for (int i = 0; i < textLenght - 1; i++, lineLength++) {
			n2 = n1.traverse();
			if (n2 == null) {
				break;
			}
			System.out.print(n2.toString());
			n1 = n2;

			if (((lineLength ) % 200) > 100 && n2.getData().contains(" ")) {
				System.out.println();
				lineLength = 1;
			}
		}
	}

	public static ZerothOrderGraph makeZerothOrderGraph(String txt) {
		
		ZerothOrderGraph g = new ZerothOrderGraph();
	
		for (int i = 0; i < txt.length(); i++) {
			g.addNode(txt.substring(i, i + 1));
		}
	
		return g;
	}

	public static void generateZerothOrderText(ZerothOrderGraph g0) {
		for (int i = 0; i < textLenght - 1; i++) {
			System.out.print(g0.getRandom());
			
			if (i + 1 % 100 == 0) {
				System.out.println();
			}
		}
		
	}

}
